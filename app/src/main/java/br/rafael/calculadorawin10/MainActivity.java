package br.rafael.calculadorawin10;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView main,hist;

    private int check=0;

    private double values1,values2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        main = this.findViewById(R.id.txtPrinc);
        hist = this.findViewById(R.id.txtHist);

    }
    public void clickBtn0 (View view){
        this.main.setText(this.main.getText()+"0");
    }
    public void clickBtn1 (View view){
        this.main.setText(this.main.getText()+"1");
    }
    public void clickBtn2 (View view){
        this.main.setText(this.main.getText()+"2");
    }
    public void clickBtn3 (View view){
        this.main.setText(this.main.getText()+"3");
    }
    public void clickBtn4 (View view) {
        this.main.setText(this.main.getText() +"4");
    }
    public void clickBtn5 (View view){
        this.main.setText(this.main.getText()+"5");
    }
    public void clickBtn6 (View view){
        this.main.setText(this.main.getText()+"6");
    }
    public void clickBtn7 (View view){
        this.main.setText(this.main.getText()+"7");
    }
    public void clickBtn8 (View view){
        this.main.setText(this.main.getText()+"8");
    }
    public void clickBtn9 (View view){
        this.main.setText(this.main.getText()+"9");
    }
    public void clickBtnDot (View view){
        if(this.check==0 ){
            if(this.main.getText().length()>=1) {
                this.main.setText(this.main.getText() + ".");
                check=1;
            }
        }

    }
    public void clickBtnClear (View view){
        main.setText("");
        hist.setText("");
        check=0;
    }


}